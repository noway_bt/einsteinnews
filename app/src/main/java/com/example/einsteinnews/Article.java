package com.example.einsteinnews;

/**
 * An {@link Article} object contains information related to a single article.
 */

public class Article {

    /** Section from which the article comes from. */
    private String mSection;

    /**Author of the article. */
    private String mAuthor;

    /** Title of the article. */
    private String mTitle;

    /** Time when the article was published. */
    private String mDate;

    /** Website URL of the article. */
    private String mUrl;

    /**
     * Constructs a new {@link Article} object.
     */
    public Article(String section, String author, String title, String date, String url) {
        mSection = section;
        mAuthor = author;
        mTitle = title;
        mDate = date;
        mUrl = url;
    }

    public Article(String section, String title, String date, String url) {
        mSection = section;
        mTitle = title;
        mDate = date;
        mUrl = url;
    }

    public String getSection() {
        return mSection;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDate() {
        return mDate;
    }

    public String getUrl() {
        return mUrl;
    }
}

